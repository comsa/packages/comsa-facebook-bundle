<?php

declare(strict_types=1);

namespace Comsa\FacebookBundle\Admin;

use Comsa\FacebookBundle\Entity\FacebookPost;
use Comsa\FacebookBundle\Entity\InstagramPost;
use Sulu\Bundle\AdminBundle\Admin\Admin;
use Sulu\Bundle\AdminBundle\Admin\Navigation\NavigationItem;
use Sulu\Bundle\AdminBundle\Admin\Navigation\NavigationItemCollection;
use Sulu\Bundle\AdminBundle\Admin\View\ToolbarAction;
use Sulu\Bundle\AdminBundle\Admin\View\ViewBuilderFactoryInterface;
use Sulu\Bundle\AdminBundle\Admin\View\ViewCollection;
use Sulu\Component\Webspace\Manager\WebspaceManagerInterface;

/**
 * Configures the Sulu Admin Backend to require the Facebook Bundle
 * @package Comsa\FacebookBundle\Admin
 */
class FacebookAdmin extends Admin {
    //-- List keys
    const FACEBOOK_POST_LIST_VIEW = "comsa_facebook_bundle.facebook_posts_list";
    const FACEBOOK_POST_LIST_KEY = "facebook_posts";
    const INSTAGRAM_POST_LIST_VIEW = "comsa_facebook_bundle.instagram_posts_list";
    const INSTAGRAM_POST_LIST_KEY = "instagram_posts";

    //-- Form keys
    const FACEBOOK_POST_EDIT_VIEW = "comsa_facebook_bundle.facebook_post_detail";
    const FACEBOOK_POST_FORM_KEY = "facebook_post_details";
    const INSTAGRAM_POST_EDIT_VIEW = "comsa_facebook_bundle.instagram_post_detail";
    const INSTAGRAM_POST_FORM_KEY = "instagram_post_details";

    private ViewBuilderFactoryInterface $viewBuilderFactory;
    private WebspaceManagerInterface $webspaceManager;

    public function __construct(ViewBuilderFactoryInterface $viewBuilderFactory, WebspaceManagerInterface $webspaceManager) {
        $this->viewBuilderFactory = $viewBuilderFactory;
        $this->webspaceManager = $webspaceManager;
    }

    public function configureNavigationItems(NavigationItemCollection $navigationItemCollection): void {
        //-- Facebook Module
        $facebookModule = new NavigationItem("comsa_facebook_bundle.facebook");
        $facebookModule->setIcon("su-newspaper");

        $facebookPostsView = new NavigationItem("comsa_facebook_bundle.facebook_posts");
        $facebookPostsView->setView(static::FACEBOOK_POST_LIST_VIEW);

        $facebookModule->addChild($facebookPostsView);

        $navigationItemCollection->add($facebookModule);

        //-- Instagram Module
        $instagramModule = new NavigationItem("comsa_facebook_bundle.instagram");
        $instagramModule->setIcon("su-newspaper");

        $instagramPostsView = new NavigationItem("comsa_facebook_bundle.instagram_posts");
        $instagramPostsView->setView(static::INSTAGRAM_POST_LIST_VIEW);

        $instagramModule->addChild($instagramPostsView);

        $navigationItemCollection->add($instagramModule);
    }

    public function configureSocialsView(ViewCollection $viewCollection): void {
        $locales = $this->webspaceManager->getAllLocales();

        //-- Facebook Module
        $facebookPostsListView =
            $this->viewBuilderFactory->createListViewBuilder(self::FACEBOOK_POST_LIST_VIEW, "/facebook_posts/:locale")
            ->setResourceKey(FacebookPost::RESOURCE_KEY)
            ->setListKey(self::FACEBOOK_POST_LIST_KEY)
            ->setTitle("comsa_facebook_bundle.facebook_posts")
            ->addLocales($locales)
            ->setDefaultLocale($locales[0])
            ->addListAdapters(["table"])
            ->addToolbarActions([
                new ToolbarAction("sulu_admin.delete")
            ])
            ->setEditView(self::FACEBOOK_POST_EDIT_VIEW);

        $facebookPostResourceTabView =
            $this->viewBuilderFactory->createResourceTabViewBuilder(self::FACEBOOK_POST_EDIT_VIEW, "/facebook_posts/:locale/:id")
            ->setResourceKey(FacebookPost::RESOURCE_KEY)
            ->setBackView(self::FACEBOOK_POST_LIST_VIEW)
            ->setTitleProperty("message")
            ->addLocales($locales);

        $facebookPostsFormView =
            $this->viewBuilderFactory->createFormViewBuilder(self::FACEBOOK_POST_EDIT_VIEW . ".details", "/details")
            ->setResourceKey(FacebookPost::RESOURCE_KEY)
            ->setFormKey(self::FACEBOOK_POST_FORM_KEY)
            ->setTabTitle("sulu_admin.details")
            ->addToolbarActions([
                new ToolbarAction("sulu_admin.save"),
            ])
            ->setParent(self::FACEBOOK_POST_EDIT_VIEW);

        $viewCollection->add($facebookPostsListView);
        $viewCollection->add($facebookPostResourceTabView);
        $viewCollection->add($facebookPostsFormView);

        //-- Instagram Module
        $instagramPostListView =
            $this->viewBuilderFactory->createListViewBuilder(self::INSTAGRAM_POST_LIST_VIEW, "/instagram_posts/:locale")
            ->setResourceKey(InstagramPost::RESOURCE_KEY)
            ->setListKey(self::INSTAGRAM_POST_LIST_KEY)
            ->setTitle("comsa_facebook_bundle.instagram_posts")
            ->addLocales($locales)
            ->setDefaultLocale($locales[0])
            ->addListAdapters(["table"])
            ->addToolbarActions([
                new ToolbarAction("sulu_admin.delete")
            ])
            ->setEditView(self::INSTAGRAM_POST_EDIT_VIEW);

        $instagramPostResourceTabView =
            $this->viewBuilderFactory->createResourceTabViewBuilder(self::INSTAGRAM_POST_EDIT_VIEW, "/instagram_posts/:locale/:id")
            ->setResourceKey(InstagramPost::RESOURCE_KEY)
            ->setBackView(self::INSTAGRAM_POST_LIST_VIEW)
            ->setTitleProperty("caption")
            ->addLocales($locales);

        $instagramPostsFormView =
            $this->viewBuilderFactory->createFormViewBuilder(self::INSTAGRAM_POST_EDIT_VIEW . ".details", "/details")
            ->setResourceKey(InstagramPost::RESOURCE_KEY)
            ->setFormKey(self::INSTAGRAM_POST_FORM_KEY)
            ->setTabTitle("sulu_admin.details")
            ->addToolbarActions([
                new ToolbarAction("sulu_admin.save")
            ])
            ->setParent(self::INSTAGRAM_POST_EDIT_VIEW);

        $viewCollection->add($instagramPostListView);
        $viewCollection->add($instagramPostResourceTabView);
        $viewCollection->add($instagramPostsFormView);
    }

    public function configureViews(ViewCollection $viewCollection): void {
        $this->configureSocialsView($viewCollection);
    }

}
