<?php

declare(strict_types=1);

namespace Comsa\FacebookBundle\Twig;

use Comsa\FacebookBundle\Entity\FacebookPost;
use Comsa\FacebookBundle\Entity\InstagramPost;
use Comsa\FacebookBundle\Repository\FacebookPostRepository;
use Comsa\FacebookBundle\Repository\InstagramPostRepository;
use Comsa\FacebookBundle\Service\FacebookPostService;
use Comsa\FacebookBundle\Service\InstagramPostService;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;
use Twig\TwigFunction;

/**
 * Configures Twig Filters and Functions
 * @package Comsa\FacebookBundle\Twig
 */
class FacebookExtension extends AbstractExtension
{

    private TranslatorInterface $translator;
    private EntityManagerInterface $entityManager;
    private FacebookPostService $facebookPostService;
    private InstagramPostService $instagramPostService;

    public function __construct(
        TranslatorInterface $translator,
        EntityManagerInterface $entityManager,
        FacebookPostService $facebookPostService,
        InstagramPostService $instagramPostService
    ) {
        $this->translator = $translator;
        $this->entityManager = $entityManager;
        $this->facebookPostService = $facebookPostService;
        $this->instagramPostService = $instagramPostService;
    }

    public function getFunctions(): array {
        return [
            new TwigFunction("unserialize_attachments", [$this, "unserializeAttachments"]),
            new TwigFunction("parse_facebook_widget", [$this, "parseFacebookWidget"])
        ];
    }

    public function unserializeAttachments($attachments): array
    {
        return unserialize($attachments);
    }

    // Type can be either Facebook or Instagram
    public function parseFacebookWidget(string $type, int $amount): array
    {
        if ($type === "facebook") {
            return $this->facebookPostService->getRepository()->findAmount($amount);
        } else {
            return $this->instagramPostService->getRepository()->findAmount($amount);
        }
    }
}
