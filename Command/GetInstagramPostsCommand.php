<?php

declare(strict_types=1);

namespace Comsa\FacebookBundle\Command;

use Comsa\FacebookBundle\Service\InstagramPostService;
use Doctrine\Common\Collections\ArrayCollection;
use PHPUnit\Exception;
use Sulu\Bundle\MediaBundle\Media\Manager\MediaManager;
use Sulu\Bundle\MediaBundle\Media\Manager\MediaManagerInterface;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Contracts\HttpClient\HttpClientInterface;

#[AsCommand(
    name: "comsa:import:instagram-posts",
    description: "Fetches Instagram Posts"
)]
class GetInstagramPostsCommand extends Command {
    private ParameterBagInterface $parameterBag;
    private HttpClientInterface $httpClient;
    private InstagramPostService $service;
    private MediaManagerInterface $mediaManager;
    private Filesystem $filesystem;

    private string $locale;
    private int $collectionId;

    public function __construct(ParameterBagInterface $parameterBag, HttpClientInterface $httpClient, InstagramPostService $service, MediaManagerInterface $mediaManager) {
        $this->parameterBag = $parameterBag;
        $this->httpClient = $httpClient;
        $this->service = $service;
        $this->mediaManager = $mediaManager;
        $this->filesystem = new Filesystem();
        parent::__construct();
    }

    protected function configure(): void {
        $this->setDescription("Fetches Instram Posts");
        $this->addArgument("locale", InputArgument::REQUIRED, "What locale for image upload?");
        $this->addArgument("collectionId", InputArgument::REQUIRED, "Media collection ID for uploading images?");
        $this->addArgument("amount", InputArgument::OPTIONAL, "How many posts do you want to retrieve?");
    }

    protected function execute(InputInterface $input, OutputInterface $output) {
        $io = new SymfonyStyle($input, $output);

        if ((int) $input->getArgument("amount")) {
            $requestAmount = (int) $input->getArgument("amount");
        } else {
            $requestAmount = 100;
        }

        $this->locale = (string) $input->getArgument("locale");
        $this->collectionId = (int) $input->getArgument("collectionId");

        $postCount = 0;
        $pageCount = 1;

        $url = sprintf(
          "%s/%s/media?fields=caption,media_url,timestamp,permalink,media_type&access_token=%s",
            $this->parameterBag->get("comsa_facebook_bundle_api_url"),
            $this->parameterBag->get("comsa_facebook_bundle_instagram_user_id"),
            $this->parameterBag->get("comsa_facebook_bundle_access_token")
        );

        $io->info(
            sprintf(
                "Import Started: %s",
                (new \DateTime())->format("d-m-Y")
            )
        );

        do {
            if ($pageCount === 1) {
                $posts = $this->getPosts($url, $io);
                foreach ($posts as $post) {
                    if ($postCount < $requestAmount) {
                        if (!$this->service->getRepository()->findOneByExternalId($post->id)) {
                            $this->createInstagramPost($post, $io);
                        }
                    }

                    $postCount++;
                }
            } else {
                $url = $this->getNextUrl($url, $io);
                $posts = $this->getPosts($url, $io);
                foreach ($posts as $post) {
                    if ($postCount < $requestAmount) {
                        if (!$this->service->getRepository()->findOneByExternalId($post->id)) {
                            $this->createInstagramPost($post, $io);
                        }
                    }

                    $postCount++;
                }
            }

            $pageCount++;

        } while ($postCount < $requestAmount);

        $io->success(
            sprintf(
                "Import ended: %s",
                (new \DateTime())->format("d-m-Y H:i:s")
            )
        );

        return Command::SUCCESS;
    }

    public function getPosts(string $url, SymfonyStyle $output): array|int {
        $response = $this->httpClient->request(
            "GET",
            $url
        );

        if ($response->getStatusCode() !== Response::HTTP_OK) {
            $output->error($response->getContent(false));
            exit();
        }

        $data = json_decode($response->getContent());

        if ($data) {
            $posts = $data->data;
            return $posts;
        } else
        {
            $this->noPostsLeft($output);
        }

    }

    public function createInstagramPost(object $post, SymfonyStyle $output): void {
        $output->info(
            sprintf(
                "New Instagram Post: %s",
                $post->id
            )
        );

        $this->service->create([
            "externalId" => $post->id,
            "caption" => $post->caption ?? "",
            "media" => isset($post->media_url) ? new ArrayCollection($this->uploadImages(explode(",", $post->media_url))) : new ArrayCollection(),
            "mediaType" => $post->media_type,
            "videoUrl" => $post->media_type === "VIDEO"  ? $post->media_url ?? null : null,
            "url" => $post->permalink,
            "createdTime" => new \DateTime($post->timestamp)
        ]);

        $output->success(
            sprintf(
                "Instagram Post imported: %s",
                $post->id
            )
        );
    }

    public function getNextUrl(string $url, SymfonyStyle $output) {
        $response = $this->httpClient->request(
            "GET",
            $url
        );

        if ($response->getStatusCode() !== Response::HTTP_OK) {
            $output->error($response->getContent(false));
            return;
        }

        $data = json_decode($response->getContent());

        if (isset($data->paging->next)) {
            return $data->paging->next;
        } else {
            $this->noPostsLeft($output);
        }
    }

    public function noPostsLeft(SymfonyStyle $output) {
        $output->info("No Posts Left");
        exit();
        return Command::SUCCESS;
    }

    private function uploadImages(array $images): array {
        $media = [];
        $data["locale"] = $this->locale;
        $data["collection"] = $this->collectionId;

        foreach ($images as $path) {
            try {
                $content = file_get_contents($path);

                $newFileName = uniqid("", true) .".jpg";
                $data["title"] = $newFileName;
                $data["name"] = $newFileName;

                $tempPath = $this->parameterBag->get("uploaded_files_directory") . $newFileName;
                file_put_contents($tempPath, $content);

                $uploadedFile = new UploadedFile($tempPath, $newFileName);
                $media[] = $this->mediaManager->save($uploadedFile, $data, 1)->setTitle($newFileName)->getEntity();

                $this->filesystem->remove($tempPath);
            } catch (\Exception $e) {
                dump($e->getMessage());
                die();
            }
        }

        return $media;
    }

}
