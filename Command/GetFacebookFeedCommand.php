<?php

declare(strict_types=1);

namespace Comsa\FacebookBundle\Command;

use Comsa\FacebookBundle\Entity\FacebookPost;
use Comsa\FacebookBundle\Factory\FacebookPostFactory;
use Comsa\FacebookBundle\Repository\FacebookPostRepository;
use Comsa\FacebookBundle\Service\FacebookPostService;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ObjectRepository;
use PHPUnit\Exception;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Contracts\HttpClient\HttpClientInterface;

/**
 * Fetches Facebook Posts
 */
#[AsCommand(
    name: "comsa:import:facebook-posts",
    description: "Fetches Facebook Posts"
)]
class GetFacebookFeedCommand extends Command {

    private ParameterBagInterface $parameterBag;
    private HttpClientInterface $httpClient;
    private FacebookPostService $facebookPostService;

    public function __construct(
        ParameterBagInterface $parameterBag,
        HttpClientInterface $httpClient,
        FacebookPostService $facebookPostService
    ) {
        $this->parameterBag = $parameterBag;
        $this->httpClient = $httpClient;
        $this->facebookPostService = $facebookPostService;

        parent::__construct();
    }

    protected function configure(): void {
        $this->setDescription("Fetches Facebook Posts");
        $this->addArgument("amount", InputArgument::OPTIONAL, "How many posts do you want to retrieve?");
    }

    protected function execute(InputInterface $input, OutputInterface $output) {
        $io = new SymfonyStyle($input, $output);

        if ((int) $input->getArgument("amount")) {
            $requestedAmount = (int) $input->getArgument("amount");
        } else {
            $requestedAmount = 100;
        }

        $postCount = 0;
        $pageCount = 1;

        $startTime = new \DateTime();
        $startTime = $startTime->format("Y-m-d H:i:s");

        $url = sprintf(
            "%s/%s/feed?access_token=%s",
            $this->parameterBag->get("comsa_facebook_bundle_api_url"),
            $this->parameterBag->get("comsa_facebook_bundle_page_id"),
            $this->parameterBag->get("comsa_facebook_bundle_access_token")
        );

        $output->writeln("<info>Started: {$startTime}</info>");
        do {
            if ($pageCount === 1) {
                $posts = $this->getPosts($url, $io);
                foreach ($posts as $post) {
                    if ($postCount < $requestedAmount) {
                        if (!$this->facebookPostService->getRepository()->findOneByExternalId($post->id)) {
                            $this->createFacebookPost($post, $io);
                        }
                    }

                    $postCount++;
                }
            } else {
                $url = $this->getNextUrl($url, $io);
                $posts = $this->getPosts($url, $io);
                foreach ($posts as $post) {
                    if ($postCount < $requestedAmount) {
                        if (!$this->facebookPostService->getRepository()->findOneByExternalId($post->id)) {
                            $this->createFacebookPost($post, $io);
                        }
                    }

                    $postCount++;
                }
            }

            $pageCount++;

        } while ($postCount < $requestedAmount);

        $io->success(
            sprintf(
                "Facebook Import completed: %s",
                (new \DateTime())->format("d-m-Y H:i:s")
            )
        );

        return Command::SUCCESS;
    }

    public function getNextUrl(string $url, SymfonyStyle $output): string {
        $response = $this->httpClient->request(
            "GET",
            $url
        );

        if ($response->getStatusCode() !== Response::HTTP_OK) {
            $io->error($response->getContent(false));
            exit();
        }

        try {
        } catch (Exception $exception) {
            $output->error($exception->getMessage());
        }

        $data = json_decode($response->getContent());

        if (isset($data->paging->next)) {
            return $data->paging->next;
        } else {
            $this->noPostsLeft($outut);
        }

    }

    public function getPosts(string $url, SymfonyStyle $output): array {
        $response = $this->httpClient->request(
            "GET",
            $url
        );

        if ($response->getStatusCode() !== Response::HTTP_OK) {
            $output->error($response->getContent(false));
            exit();
        }

        $data = json_decode($response->getContent());

        if ($data) {
            $posts = $data->data;
        } else {
            $this->noPostsLeft($io);
        }

        return $posts;
    }

    public function createFacebookPost(object $post, SymfonyStyle $io): void {
        $io->info(sprintf("New Facebook Post: %s", $post->id));
        $this->facebookPostService->create([
           "externalId" => $post->id,
           "message" => isset($post->message) ? $post->message : null,
           "attachments" => $this->getAttachments($post, $io),
           "createdTime" => new \DateTime($post->created_time)
        ]);

        $io->success(sprintf("Facebook Post '%s' was succesfully added.", $post->id));
    }

    private function getAttachments(object $post, SymfonyStyle $io): ?string {
        $url = sprintf(
            "%s/%s?fields=attachments&access_token=%s",
            $this->parameterBag->get("comsa_facebook_bundle_api_url"),
            $post->id,
            $this->parameterBag->get("comsa_facebook_bundle_access_token")
        );

        $response = $this->httpClient->request(
            "GET",
            $url
        );

        if ($response->getStatusCode() !== Response::HTTP_OK) {
            $io->error($response->getContent(false));
            return null;
        }

        $data = json_decode($response->getContent());

        if (isset($data->attachments)) {
            $attachments = $data->attachments->data;

            $fbPostMedia = [];

            foreach ($attachments as $attachment) {
                if (isset($attachment->subattachments)) {
                    $subattachments = $attachment->subattachments->data;

                    foreach ($subattachments as $subattachment) {
                        if (isset($subattachment->media->image)) {
                            $fbPostMedia[] = $subattachment->media->image->src;
                        }

                        if (isset($subattachment->media->source)) {
                            $fbPostMedia[] = $subattachment->media->source;
                        }
                    }
                } else {
                    if (isset($attachment->media->image)) {
                        $fbPostMedia[] = $attachment->media->image->src;
                    }

                    if (isset($attachment->media->source)) {
                        $fbPostMedia[] = $attachment->media->source;
                    }
                }
            }

            return serialize($fbPostMedia);
        } else {
            return null;
        }
    }

    public function noPostsLeft(SymfonyStyle $io): void {
        $io->success("No posts left");
        exit();
    }
}
