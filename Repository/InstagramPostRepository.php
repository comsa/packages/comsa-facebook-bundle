<?php

declare(strict_types=1);

namespace Comsa\FacebookBundle\Repository;

use Comsa\FacebookBundle\Entity\InstagramPost;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method InstagramPost|null find($id, $lockMode = null, $lockVersion = null)
 * @method InstagramPost|null findOneBy(array $criteria, array $orderBy = null)
 * @method InstagramPost[]    findAll()
 * @method InstagramPost[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class InstagramPostRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry) {
        parent::__construct($registry, InstagramPost::class);
    }

    public function findOneByExternalId(string $instagramId): ?InstagramPost {
        return $this->createQueryBuilder("ip")
            ->where("ip.instagramPostId = :externalId")
            ->setParameter("externalId", $instagramId)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }

    public function findAmount(int $amount): array {
        return $this->createQueryBuilder("ip")
            ->where("ip.hidden = 0")
            ->orderBy("ip.createdTime", "DESC")
            ->setMaxResults($amount)
            ->getQuery()
            ->getResult();
    }
}
