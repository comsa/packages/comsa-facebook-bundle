<?php

declare(strict_types=1);

namespace Comsa\FacebookBundle\Repository;

use Comsa\FacebookBundle\Entity\FacebookPost;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Mapping;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method FacebookPost|null find($id, $lockMode = null, $lockVersion = null)
 * @method FacebookPost|null findOneBy(array $criteria, array $orderBy = null)
 * @method FacebookPost[]    findAll()
 * @method FacebookPost[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FacebookPostRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry) {
        parent::__construct($registry, FacebookPost::class);
    }

    public function findOneByExternalId(string $facebookPostId): ?FacebookPost {
        return $this->createQueryBuilder("fbp")
            ->where("fbp.facebookPostId = :externalId")
            ->setParameter("externalId", $facebookPostId)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }

    public function findAmount(int $amount): array {
        return $this->createQueryBuilder("fbp")
            ->where("fbp.hidden = 0")
            ->orderBy("fbp.createdTime", "DESC")
            ->setMaxResults($amount)
            ->getQuery()
            ->getResult();
    }
}
