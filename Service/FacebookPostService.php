<?php

declare(strict_types=1);

namespace Comsa\FacebookBundle\Service;

use Comsa\FacebookBundle\Entity\FacebookPost;
use Comsa\FacebookBundle\Entity\Interfaces\CrudResource;
use Comsa\FacebookBundle\Factory\FacebookPostFactory;
use Comsa\FacebookBundle\Repository\FacebookPostRepository;
use Comsa\FacebookBundle\Service\Interfaces\BaseCrudServiceInterface;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\EntityManagerInterface;

class FacebookPostService extends AbstractCrudService implements BaseCrudServiceInterface {
    public function __construct(EntityManagerInterface $entityManager, FacebookPostRepository $repository) {
        parent::__construct($entityManager, $repository, FacebookPost::class);
    }

    public function create(array $data): void {
        $post = FacebookPostFactory::create(
            $data["externalId"],
            $data["message"],
            $data["attachments"],
            $data["createdTime"]
        );

        $this->save($post);
    }

    public function hide(FacebookPost $facebookPost) {
        $facebookPost->hide();
        $this->save($facebookPost);
    }
}
