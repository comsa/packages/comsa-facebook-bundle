<?php

declare(strict_types=1);

namespace Comsa\FacebookBundle\Service\Interfaces;

interface BaseCrudServiceInterface {
    public function create(array $data): void;
}
