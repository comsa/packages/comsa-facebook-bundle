<?php

declare(strict_types=1);

namespace Comsa\FacebookBundle\Service\Interfaces;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Comsa\FacebookBundle\Entity\Interfaces\CrudResource;

interface BaseOrmManagementInterface {
    public function getRepository(): ServiceEntityRepository;
    public function save(CrudResource $entity): void;
    public function delete(CrudResource $entity): void;
    public function validateEntity(CrudResource $entity): void;
}
