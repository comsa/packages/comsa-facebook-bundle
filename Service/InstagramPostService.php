<?php

declare(strict_types=1);

namespace Comsa\FacebookBundle\Service;

use Comsa\FacebookBundle\Entity\InstagramPost;
use Comsa\FacebookBundle\Entity\Interfaces\CrudResource;
use Comsa\FacebookBundle\Factory\InstagramPostFactory;
use Comsa\FacebookBundle\Repository\InstagramPostRepository;
use Comsa\FacebookBundle\Service\Interfaces\BaseCrudServiceInterface;
use Doctrine\ORM\EntityManagerInterface;

class InstagramPostService extends AbstractCrudService implements BaseCrudServiceInterface {
    public function __construct(EntityManagerInterface $entityManager, InstagramPostRepository $repository) {
        parent::__construct($entityManager, $repository, InstagramPost::class);
    }

    public function create(array $data): void {
        $post = InstagramPostFactory::create(
            $data["externalId"],
            $data["caption"],
            $data["media"],
            $data["mediaType"],
            $data["url"],
            $data["videoUrl"],
            $data["createdTime"]
        );

        $this->save($post);
    }

    public function hide(InstagramPost $post): void {
        $post->hide();
        $this->save($post);
    }
}
