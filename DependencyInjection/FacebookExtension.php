<?php

declare(strict_types=1);

namespace Comsa\FacebookBundle\DependencyInjection;

use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Extension\Extension;
use Symfony\Component\DependencyInjection\Extension\PrependExtensionInterface;
use Symfony\Component\DependencyInjection\Loader\FileLoader;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;

/**
 * Configures the Dependency Injection for the FacebookBundle
 */
class FacebookExtension extends Extension implements  PrependExtensionInterface {
    public function load(array $configs, ContainerBuilder $container)
    {
        $loader = new YamlFileLoader($container, new FileLocator(__DIR__ . "/../Resources/config"));
        $loader->load("services.yaml");
    }

    public function prepend(ContainerBuilder $container)
    {
        if ($container->hasExtension("fos_js_routing")) {
            $container->prependExtensionConfig(
                "fos_js_routing",
                [
                    "routes_to_expose" => [
                        "comsa.facebook_bundle.get_facebook_posts",
                        "comsa.facebook_bundle.get_facebook_post",
                        "comsa.facebook_bundle.get_instagram_posts",
                        "comsa.facebook_bundle.get_instagram_post"
                    ]
                ]
            );
        }

        if ($container->hasExtension("sulu_admin"))
        {
            $container->prependExtensionConfig(
                "sulu_admin",
                [
                    "lists" => [
                        "directories" => [
                            __DIR__ . "/../Resources/config/lists"
                        ]
                    ],
                    "forms" => [
                        "directories" => [
                            __DIR__ . "/../Resources/config/forms"
                        ]
                    ],
                    "resources" => [
                        "facebook_posts" => [
                            "routes" => [
                                "list" => "comsa.facebook_bundle.get_facebook_posts",
                                "detail" => "comsa.facebook_bundle.get_facebook_post"
                            ]
                        ],
                        "instagram_posts" => [
                            "routes" => [
                                "list" => "comsa.facebook_bundle.get_instagram_posts",
                                "detail" => "comsa.facebook_bundle.get_instagram_post"
                            ]
                        ]
                    ],
                    "field_type_options" => [
                        "selection" => [
                            "facebook_post_selection" => [
                                "default_type" => "list_overlay",
                                "resource_key" => "facebook_posts",
                                "types" => [
                                    "list_overlay" => [
                                        "adapter" => "table",
                                        "list_key" => "facebook_posts",
                                        "display_properties" => [
                                            "createdTime", "message"
                                        ],
                                        "label" => "Facebook Posts" ,
                                        "icon" => "su-list-ul",
                                        "overlay_title" => "Select Facebook Posts"
                                    ]
                                ]
                            ],
                            "instagram_post_selection" => [
                                "default_type" => "list_overlay",
                                "resource_key" => "instagram_posts",
                                "types" => [
                                    "list_overlay" => [
                                        "adapter" => "table",
                                        "list_key" => "instagram_posts",
                                        "display_properties" => [
                                            "createdTime", "caption"
                                        ],
                                        "label" => "Instagram Posts",
                                        "icon" => "su-list-ul",
                                        "overlay_title" => "Select Instagram Posts"
                                    ]
                                ]
                            ]
                        ]
                    ]
                ]
            );
        }

    }
}
