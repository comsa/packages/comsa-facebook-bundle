<?php

declare(strict_types=1);

namespace Comsa\FacebookBundle\Content\Types;

use Comsa\FacebookBundle\Entity\InstagramPost;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Mapping\Entity;
use Jackalope\Node;
USE PHPCR\NodeInterface;
use Sulu\Component\Content\Compat\PropertyInterface;
use Sulu\Component\Content\ComplexContentType;
use Sulu\Component\Content\PreResolvableContentTypeInterface;

/**
 * Configures a Content Type for Instagram Posts
 * @package Comsa\FacebookBundle\Content\Types
 */
class InstagramPostSelection extends ComplexContentType {
    private EntityManagerInterface $entityManager;

    public function __construct(EntityManagerInterface $entityManager) {
        $this->entityManager = $entityManager;
    }

    public function read(NodeInterface $node, PropertyInterface $property, $webspaceKey, $languageCode, $segmentKey) {
        $instagramPostIds = $node->getPropertyValueWithDefault($property->getName(), []);
        $property->setValue($instagramPostIds);
    }

    public function write(NodeInterface $node, PropertyInterface $property, $userId, $webspaceKey, $languageCode, $segmentKey) {
        $instagramPostIds = [];
        $value = $property->getValue();

        if ($value === null) {
            $node->setProperty($property->getName(), null);
            return;
        }

        foreach ($value as $instagramPost) {
            if (is_numeric($instagramPost)) {
                $instagramPostIds[] = $instagramPost;
            }
            else {
                $instagramPostIds[] = $instagramPost['id'];
            }
        }

        $node->setProperty($property->getName(), $instagramPostIds);
    }

    public function remove(NodeInterface $node, PropertyInterface $property, $webspaceKey, $languageCode, $segmentKey){
        if ($node->hasProperty($property->getName())) {
            $property = $node->getProperty($property->getName());
            $property->remove();
        }
    }

    public function getContentData(PropertyInterface $property) {
        $instagramPosts = $property->getValue();

        if ($instagramPosts === null) {
            return;
        }

        foreach ($instagramPosts as &$instagramPost) {
            $instagramPost = $this->entityManager->getRepository(InstagramPost::class)->find($instagramPost);
        }

        return $instagramPosts;
    }
}
