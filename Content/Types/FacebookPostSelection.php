<?php

declare(strict_types=1);

namespace Comsa\FacebookBundle\Content\Types;

use Comsa\FacebookBundle\Entity\FacebookPost;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Mapping\Entity;
use Jackalope\Node;
USE PHPCR\NodeInterface;
use Sulu\Component\Content\Compat\PropertyInterface;
use Sulu\Component\Content\ComplexContentType;
use Sulu\Component\Content\PreResolvableContentTypeInterface;

/**
 * Configures a Content Type for Facebook Posts
 * @package Comsa\FacebookBundle\Content\Types
 */
class FacebookPostSelection extends ComplexContentType {
    private EntityManagerInterface $entityManager;

    public function __construct(EntityManagerInterface $entityManager) {
        $this->entityManager = $entityManager;
    }

    public function read(NodeInterface $node, PropertyInterface $property, $webspaceKey, $languageCode, $segmentKey) {
        $fbPostIds = $node->getPropertyValueWithDefault($property->getName(), []);
        $property->setValue($fbPostIds);
    }

    public function write(NodeInterface $node, PropertyInterface $property, $userId, $webspaceKey, $languageCode, $segmentKey) {
        $fbPostIds = [];
        $value = $property->getValue();

        if ($value === null) {
            $node->setProperty($property->getName(), null);
            return;
        }

        foreach ($value as $fbPost) {
            if (is_numeric($fbPost)) {
                $fbPostIds[] = $fbPost;
            }
            else {
                $fbPostIds[] = $fbPost['id'];
            }
        }

        $node->setProperty($property->getName(), $fbPostIds);
    }

    public function remove(NodeInterface $node, PropertyInterface $property, $webspaceKey, $languageCode, $segmentKey) {
        if ($node->hasProperty($property->getName()))  {
            $property = $node->getProperty($property->getName());
            $property->remove();
        }
    }

    public function getContentData(PropertyInterface $property)
    {
        $fbPosts = $property->getValue();

        if ($fbPosts === null) {
            return;
        }

        foreach ($fbPosts as &$fbPost) {
            $fbPost = $this->entityManager->getRepository(FacebookPost::class)->find($fbPost);
        }

        return $fbPosts;
    }
}
