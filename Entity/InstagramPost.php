<?php

declare(strict_types=1);

namespace Comsa\FacebookBundle\Entity;

use Comsa\FacebookBundle\Entity\Interfaces\CrudResource;
use Comsa\FacebookBundle\Repository\InstagramPostRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\InverseJoinColumn;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\JoinTable;
use Doctrine\ORM\Mapping\ManyToMany;
use Doctrine\ORM\Mapping\Table;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;
use Sulu\Bundle\MediaBundle\Entity\Media;

#[
    Entity(),
    Table(name: "comsa_fb_instagram_post"),
    ExclusionPolicy("all")
]
class InstagramPost implements CrudResource {
    const RESOURCE_KEY = "instagram_posts";

    #[
        Id(),
        GeneratedValue(strategy: "AUTO"),
        Column(type: Types::INTEGER),
        Expose()
    ]
    private int $id;

    #[
        Column(type: Types::STRING, length: 255, unique: true),
        Expose()
    ]
    private string $instagramPostId;

    #[
        Column(type: Types::TEXT, length: 65535, nullable: true),
        Expose()
    ]
    private string $caption;

    #[
        Column(type: Types::DATETIME_MUTABLE),
        Expose()
    ]
    private \DateTime $createdTime;

    #[
        ManyToMany(targetEntity: "Sulu\Bundle\MediaBundle\Entity\Media"),
        JoinTable(name: "comsa_fb_instagram_media"),
        JoinColumn(name: "instagram_post_id", referencedColumnName: "id"),
        InverseJoinColumn(name: "media_id", referencedColumnName: "id"),
        Expose()
    ]
    private Collection $media;

    #[
        Column(type: Types::STRING, length: 255, nullable: true),
        Expose()
    ]
    private string $mediaType;

    #[
        Column(type: Types::TEXT, length: 65000, nullable: true),
        Expose()
    ]
    private ?string $videoUrl;

    #[
        Column(type: Types::BOOLEAN),
        Expose()
    ]
    private bool $hidden;

    #[
        Column(type: Types::TEXT, nullable: true),
        Expose()
    ]
    private string $url;

    public function __construct() {
        $this->hidden = false;
        $this->media = new ArrayCollection();
    }

    public function getId(): ?int {
        return $this->id;
    }

    public function getInstagramPostId(): ?string {
        return $this->instagramPostId;
    }

    public function setInstagramPostId(string $instagramPostId): self {
        $this->instagramPostId = $instagramPostId;

        return $this;
    }

    public function getCaption(): ?string {
        return $this->caption;
    }

    public function setCaption(string $caption): self {
        $this->caption = $caption;

        return $this;
    }

    public function getCreatedTime(): ?\DateTime {
        return $this->createdTime;
    }

    public function setCreatedTime(\DateTime $createdTime): self {
        $this->createdTime = $createdTime;

        return $this;
    }

    public function getMedia(): Collection {
        return $this->media;
    }

    public function setMedia(Collection $media): self {
        $this->media = $media;

        return $this;
    }

    public function addMedia(Media $media): self {
        if (!$this->media->contains($media)) {
            $this->media->add($media);
        }
    }

    public function getMediaType(): ?string {
        return $this->mediaType;
    }

    public function setMediaType(string $mediaType): self {
        $this->mediaType = $mediaType;

        return $this;
    }

    public function isHidden(): bool {
        return $this->hidden;
    }

    public function hide(): void {
        $this->hidden = true;
    }

    public function show(): void {
        $this->hidden = false;
    }

    public function getUrl(): ?string {
        return $this->url;
    }

    public function setUrl(?string $url): self {
        $this->url = $url;

        return $this;
    }

    public function getVideoUrl(): string {
        return $this->videoUrl;
    }

    public function setVideoUrl(?string $videoUrl): self {
        $this->videoUrl = $videoUrl;

        return $this;
    }
}
