<?php

declare(strict_types=1);

namespace Comsa\FacebookBundle\Entity;

use Comsa\FacebookBundle\Entity\Interfaces\CrudResource;
use Comsa\FacebookBundle\Repository\FacebookPostRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\Table;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;

#[
    Entity(),
    Table(name: "comsa_fb_facebook_post"),
    ExclusionPolicy("all")
]
class FacebookPost implements CrudResource {
    const RESOURCE_KEY = "facebook_posts";

    #[
        Id(),
        GeneratedValue(strategy: "AUTO"),
        Column(type: Types::INTEGER),
        Expose()
    ]
    private int $id;

    #[
        Column(type: Types::STRING, length: 255, unique: true),
        Expose()
    ]
    private string $facebookPostId;

    #[
        Column(type: Types::TEXT, length: 65535, nullable: true),
        Expose()
    ]
    private ?string $message;

    #[
        Column(type: Types::DATETIME_MUTABLE),
        Expose()
    ]
    private \DateTime $createdTime;

    #[
        Column(type: Types::JSON, nullable: true),
        Expose()
    ]
    private ?string $attachments;

    #[
        Column(type: Types::TEXT, length: 65535, nullable: true),
        Expose()
    ]
    private ?string $url;

    #[
        Column(type: Types::BOOLEAN),
        Expose()
    ]
    private bool $hidden;

    public function __construct() {
        $this->hidden = false;
    }

    public function getId(): ?int {
        return $this->id;
    }

    public function getFacebookPostId(): ?string {
        return $this->facebookPostId;
    }

    public function setFacebookPostId(string $facebookPostId): self {
        $this->facebookPostId = $facebookPostId;
        return $this;
    }

    public function getMessage(): ?string {
        return $this->message;
    }

    public function setMessage(?string $message): self {
        $this->message = $message;

        return $this;
    }

    public function getCreatedTime(): ?\DateTimeInterface {
        return $this->createdTime;
    }

    public function setCreatedTime(\DateTimeInterface $createdTime): self {
        $this->createdTime = $createdTime;

        return $this;
    }

    public function getAttachments() {
        return $this->attachments;
    }

    public function setAttachments(?string $attachments): self {
        $this->attachments = $attachments;

        return $this;
    }

    public function getUrl(): ?string {
        return $this->url;
    }

    public function setUrl(?string $url): self {
        $this->url = $url;

        return $this;
    }

    public function isHidden(): bool {
        return $this->hidden;
    }

    public function hide(): void {
        $this->hidden = true;
    }

    public function show(): void {
        $this->hidden = false;
    }
}
