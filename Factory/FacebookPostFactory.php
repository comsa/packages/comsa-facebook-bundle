<?php

declare(strict_types=1);

namespace Comsa\FacebookBundle\Factory;

use Comsa\FacebookBundle\Entity\FacebookPost;

/**
 * Responsible for creating Facebook Post Objects
 * @package Comsa\FacebookBundle\Factory
 */
class FacebookPostFactory {
    public static function create(
        string $facebookPostId,
        ?string $message,
        ?string $attachments,
        \DateTime $createdTime
    ): FacebookPost {
        return (new FacebookPost())
            ->setFacebookPostId($facebookPostId)
            ->setMessage($message)
            ->setAttachments($attachments)
            ->setCreatedTime($createdTime)
        ;
    }
}
