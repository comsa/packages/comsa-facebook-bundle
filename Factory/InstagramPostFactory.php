<?php

declare(strict_types=1);

namespace Comsa\FacebookBundle\Factory;

use Comsa\FacebookBundle\Entity\InstagramPost;
use Doctrine\Common\Collections\Collection;

/**
 * Responsible for creating Instagram Post Objects
 * @package Comsa\FacebookBundle\Factory
 */
class InstagramPostFactory
{
    public static function create(
        string $instagramPostId,
        string $caption,
        Collection $media,
        string $mediaType,
        string $url,
        ?string $videoUrl,
        \DateTime $createdTime
    ): InstagramPost {
        return (new InstagramPost())
            ->setInstagramPostId($instagramPostId)
            ->setCaption($caption)
            ->setMedia($media)
            ->setMediaType($mediaType)
            ->setUrl($url)
            ->setVideoUrl($videoUrl)
            ->setCreatedTime($createdTime)
        ;
    }
}
