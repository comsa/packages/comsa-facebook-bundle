<?php

declare(strict_types=1);

namespace Comsa\FacebookBundle\Controller\Admin;

use Comsa\FacebookBundle\Entity\FacebookPost;
use Comsa\FacebookBundle\Service\FacebookPostService;
use FOS\RestBundle\View\ViewHandlerInterface;
use HandcraftedInTheAlps\RestRoutingBundle\Routing\ClassResourceInterface;
use Sulu\Component\Rest\AbstractRestController;
use Sulu\Component\Rest\ListBuilder\Doctrine\DoctrineListBuilderFactoryInterface;
use Sulu\Component\Rest\ListBuilder\ListRepresentation;
use Sulu\Component\Rest\ListBuilder\Metadata\FieldDescriptorFactoryInterface;
use Sulu\Component\Rest\RestHelperInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Configures the REST API for Facebook Posts
 * @package Comsa\FacebookBundle\Controller\Admin
 */
class FacebookPostController extends AbstractRestController implements ClassResourceInterface {

    private DoctrineListBuilderFactoryInterface $doctrineListBuilderFactory;
    private FieldDescriptorFactoryInterface $fieldDescriptorFactory;
    private RestHelperInterface $restHelper;
    private FacebookPostService $service;

    public function __construct
    (
        ViewHandlerInterface $viewHandler,
        DoctrineListBuilderFactoryInterface $doctrineListBuilderFactory,
        FieldDescriptorFactoryInterface $fieldDescriptorFactory,
        RestHelperInterface $restHelper,
        FacebookPostService $service
    ) {
        $this->doctrineListBuilderFactory = $doctrineListBuilderFactory;
        $this->fieldDescriptorFactory = $fieldDescriptorFactory;
        $this->restHelper = $restHelper;
        $this->service = $service;

        parent::__construct($viewHandler);
    }

    public function cgetAction(Request $request): Response {
        $listbuilder = $this->doctrineListBuilderFactory->create(FacebookPost::class);
        $fieldDescriptors = $this->fieldDescriptorFactory->getFieldDescriptors(FacebookPost::RESOURCE_KEY);
        $listbuilder->where($fieldDescriptors["hidden"], false);
        $listbuilder->sort($fieldDescriptors["created_time"], "DESC");

        $this->restHelper->initializeListBuilder($listbuilder, $fieldDescriptors);

        $representation = new ListRepresentation(
            $listbuilder->execute(),
            "facebook_posts",
            $request->get("_route"),
            $request->query->all(),
            $listbuilder->getCurrentPage(),
            $listbuilder->getLimit(),
            $listbuilder->count()
        );

        return $this->handleView($this->view($representation));
    }

    public function getAction(int $id, Request $request): Response {
        /** @var FacebookPost $fbPost */
        $fbPost = $this->service->getRepository()->find($id);

        if (!$fbPost) {
            throw new NotFoundHttpException();
        }

        $attachments = $fbPost->getAttachments();
        $fbPost->setAttachments(implode('<br><br>' ,unserialize($attachments)));

        return $this->handleView($this->view($fbPost));
    }

    public function deleteAction(int $id, Request $request): Response {
        /** @var FacebookPost $fbPost */
        $fbPost = $this->service->getRepository()->find($id);

        if (!$fbPost) {
            throw new NotFoundHttpException();
        }

        $this->service->hide($fbPost);

        return $this->handleView($this->view());
    }
}
