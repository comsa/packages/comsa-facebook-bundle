<?php

declare(strict_types=1);

namespace Comsa\FacebookBundle\Controller\Admin;

use Comsa\FacebookBundle\Entity\InstagramPost;
use Comsa\FacebookBundle\Service\InstagramPostService;
use Sulu\Bundle\MediaBundle\Media\Manager\MediaManagerInterface;
use Sulu\Component\Rest\AbstractRestController;
use HandcraftedInTheAlps\RestRoutingBundle\Routing\ClassResourceInterface;
use FOS\RestBundle\View\ViewHandlerInterface;
use Sulu\Component\Rest\ListBuilder\Doctrine\DoctrineListBuilderFactoryInterface;
use Sulu\Component\Rest\ListBuilder\ListRepresentation;
use Sulu\Component\Rest\ListBuilder\Metadata\FieldDescriptorFactoryInterface;
use Sulu\Component\Rest\RestHelperInterface;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use ZendSearch\Lucene\Document\Field;

/**
 * Configures the REST API for Instagram Posts
 * @package Comsa\FacebookBundle\Controller\Admin
 */
class InstagramPostController extends AbstractRestController implements ClassResourceInterface {

    private DoctrineListBuilderFactoryInterface $doctrineListBuilderFactory;
    private FieldDescriptorFactoryInterface $fieldDescriptorFactory;
    private RestHelperInterface $restHelper;
    private InstagramPostService $service;

    public function __construct
    (
        ViewHandlerInterface $viewHandler,
        DoctrineListBuilderFactoryInterface $doctrineListBuilderFactory,
        FieldDescriptorFactoryInterface $fieldDescriptorFactory,
        RestHelperInterface $restHelper,
        InstagramPostService $service
    ) {
        $this->doctrineListBuilderFactory = $doctrineListBuilderFactory;
        $this->fieldDescriptorFactory = $fieldDescriptorFactory;
        $this->restHelper = $restHelper;
        $this->service = $service;

        parent::__construct($viewHandler);
    }

    public function cgetAction(Request $request): Response {
        $locale = $this->getLocale($request);

        $listbuilder = $this->doctrineListBuilderFactory->create(InstagramPost::class);
        $fieldDescriptors = $this->fieldDescriptorFactory->getFieldDescriptors(InstagramPost::RESOURCE_KEY);
        $listbuilder->sort($fieldDescriptors["created_time"], "DESC");
        $listbuilder->where($fieldDescriptors["hidden"], false);

        $this->restHelper->initializeListBuilder($listbuilder, $fieldDescriptors);

        $representation = new ListRepresentation(
            $listbuilder->execute(),
            "instagram_posts",
            $request->get("_route"),
            $request->query->all(),
            $listbuilder->getCurrentPage(),
            $listbuilder->getLimit(),
            $listbuilder->count()
        );

        return $this->handleView($this->view($representation));
    }

    public function getAction(int $id, Request $request): Response {
        /** @var InstagramPost $instagramPost */
        $instagramPost = $this->service->getRepository()->find($id);

        if (!$instagramPost) {
            throw new NotFoundHttpException();
        }

        return $this->handleView($this->view($instagramPost));
    }

    public function deleteAction(int $id, Request $request): Response {
        /** @var InstagramPost $instagramPost */
        $instagramPost = $this->service->getRepository()->find($id);

        if (!$instagramPost) {
            throw new NotFoundHttpException();
        }

        $this->service->hide($instagramPost);

        return $this->handleView($this->view());
    }
}
